function doReduce (arr, func, initial) {
    
    if (Array.isArray(arr) && arr.length > 0) {
        if (initial !== undefined) {
            let result = initial;

            for (let index = 0; index < arr.length; index++) {
                result = func(initial, arr[index], index, arr);
                initial = result;
            }
    
        return result;

        } else {
            let initial = arr[0];
            let result = initial;

            for (let index = 1; index < arr.length; index++) {
                result = func(initial, arr[index], index, arr);
                initial = result;
            }
        
            return result;
        }

    } else {
        return [];
    }

}

module.exports = doReduce;
