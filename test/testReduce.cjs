// Importing modules.
let doReduce = require ('../reduce.cjs');
// Given array data.
const items = [1, 2, 3, 4, 5, 5];
// Function to get sum of elements.
function sum (prev, curr, index, elements) {
    let sums;
    return  sums = curr + prev;
    
}
// Function to find minimum of elements.
function minimum (prev, curr, index, elements) {
    if (prev < curr) {

        return curr = prev;

    } else {

        return prev;
    }
}
// Applying function to get desired result.
const sumOfArray = doReduce(items, sum, 0);
const minOfArray = doReduce(items, minimum);
// Printing the result.
console.log(sumOfArray);
console.log(minOfArray);
