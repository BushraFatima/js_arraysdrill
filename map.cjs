function doMap (arr, func) {

    if (Array.isArray(arr)) {
        let newArr = [];

        for (let num = 0; num < arr.length; num++) {
    
            newArr.push(func(arr[num], num, arr));
        }
        
        return newArr;

    } else {
        return 'Array not found';
    }
 
}

module.exports = doMap;

