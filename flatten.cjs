function doFlat(items, depth) {
    let result = [];

    if (depth === Infinity) {

        if (!Array.isArray(items)) {
            return items;
        }
    } 

    for (let index = 0; index < items.length; index++) {

        if (items[index] === undefined) {
            continue;

        } else {

            if (depth === Infinity) {
                result = result.concat(doFlat(items[index], Infinity));

            }else if (depth === undefined || depth === null || depth === 1){

               if (!Array.isArray(items[index])) {
                    result = result.concat(items[index]);

                } else {
                    let itemArr = [];

                    for (let num = 0; num < items[index].length; num++) {

                        if (items[index][num] === undefined) {
                            continue;
                        } else {
                            itemArr.push(items[index][num]);
                        }
                    }

                    result = result.concat(itemArr);
                }

            } else {

                if (!Array.isArray(items[index])) {
                    result = result.concat(items[index]);
                } else {
                    result = result.concat(doFlat(items[index], depth-1));
                }
                
            }
 
        }
         
    }

    return result;
}

module.exports = doFlat;
